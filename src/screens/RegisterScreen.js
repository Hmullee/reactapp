import React, { useState, useEffect } from "react";
import { View, Text, SafeAreaView, StyleSheet } from "react-native";
import FormInput from "../components/FormInput";
import Button from "../components/Button";
import { useDispatch, useSelector } from "react-redux";
import { registerInitiate } from "../redux/actions/authActions";


export default function Register({ navigation }) {
  const [name, setName] = useState("");
  const [email, setEmail] = useState("");
  const [password, setPassword] = useState("");
  
  const dispatch = useDispatch();
  const { user} = useSelector((state) => state.auth);

  useEffect(() => {
    if (user) {
      navigation.navigate("Quote");
    }
  }, [user]);

  const register = () => {
    dispatch(registerInitiate(name, email, password))
  };

  return (
    <SafeAreaView>
      <View style={styles.container}>
        <Text style={styles.loginHeader}>Register</Text>
        <FormInput placeholder="Name" value={name} setValue={setName} />
        <FormInput placeholder="Email" value={email} setValue={setEmail} />
        <FormInput
          placeholder="Password"
          value={password}
          setValue={setPassword}
        />
        <Button text="Register" onPress={register} />
        <Button
          text="Already have an account?"
          onPress={() => navigation.navigate("Login")}
          type="TERTIARY"
        />
      </View>
    </SafeAreaView>
  );
}

const styles = StyleSheet.create({
  container: {
    alignItems: "center",
    padding: 20,
  },
  loginHeader: {
    fontSize: 30,
    fontWeight: "bold",
    padding: 20,
  },
});
