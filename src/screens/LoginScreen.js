import React, { useEffect, useState } from "react";
import { View, Text, StyleSheet, ScrollView, SafeAreaView } from "react-native";
import Button from "../components/Button";
import FormInput from "../components/FormInput";
import { useDispatch, useSelector } from "react-redux";
import { loginInitiate } from "../redux/actions/authActions";

export default function LoginScreen({ navigation }) {
  const [email, setEmail] = useState("");
  const [password, setPassword] = useState("");

  const dispatch = useDispatch();
  const { token } = useSelector((state) => state.auth);

  useEffect(() => {
    if (token) {
      navigation.navigate("Quote");
    }
  }, [token]);

  const login = () => {
    dispatch(loginInitiate(email, password));
    clearLoginForm();
  };

  const clearLoginForm = () => {
    setEmail("");
    setPassword("");
  };

  return (
    <SafeAreaView>
      <ScrollView>
        <View style={styles.container}>
          <Text style={styles.loginHeader}>Login</Text>
          <FormInput placeholder="Email" value={email} setValue={setEmail} />
          <FormInput
            placeholder="Password"
            value={password}
            setValue={setPassword}
            secureTextEntry={true}
          />
          <Button text="Sign In" onPress={login} />

          <Button
            text="Need an account?"
            onPress={() => navigation.navigate("Register")}
            type="TERTIARY"
          />
        </View>
      </ScrollView>
    </SafeAreaView>
  );
}

const styles = StyleSheet.create({
  container: {
    alignItems: "center",
    padding: 20,
  },
  loginHeader: {
    fontSize: 30,
    fontWeight: "bold",
    padding: 20,
  },
});
