import React, { useState } from "react";
import { View, Text, SafeAreaView, StyleSheet, ScrollView } from "react-native";
import { useSelector, useDispatch, useEffect } from "react-redux";
import Button from "../components//Button";
import FormInput from "../components/FormInput";
import { logoutUser, setHeader } from "../redux/actions/authActions";
import DateTimePicker from "@react-native-community/datetimepicker";
import { Dropdown } from "react-native-material-dropdown-v2";
import { getQuote } from "../redux/actions/quoteActions";

export default function QuoteScreen({ navigation }) {
  const [ages, setAges] = useState();
  const [currency, setCurrency] = useState("");
  const [startDate, setStartDate] = useState(new Date());
  const [endDate, setEndDate] = useState(new Date());
  const [mode, setMode] = useState("date");
  const [startDateShow, setStartDateShow] = useState(false);
  const [endDateShow, setEndDateShow] = useState(false);
  const [showTotal, setShowTotal] = useState(false);

  const { quoteCurrency, quoteId, quoteTotal } = useSelector(
    (state) => state.quote
  );
  const { token } = useSelector((state) => state.auth);
  let dispatch = useDispatch();

  const logout = () => {
    dispatch(logoutUser());
    navigation.navigate("Login");
  };

  const onStartDateChange = (event, selectedDate) => {
    const currentDate = selectedDate || startDate;
    setStartDateShow(Platform.OS === "ios");
    setStartDate(currentDate);
  };

  const onEndDateChange = (event, selectedDate) => {
    const currentDate = selectedDate || endDate;
    setEndDateShow(Platform.OS === "ios");
    setEndDate(currentDate);
  };

  const showMode = (currentMode) => {
    setMode(currentMode);
  };

  const showStartDatepicker = () => {
    setStartDateShow(!startDateShow);
    showMode("date");
  };

  const showEndDatepicker = () => {
    setEndDateShow(!endDateShow);
    showMode("date");
  };

  let data = [
    {
      value: "USD",
    },
    {
      value: "GDP",
    },
    {
      value: "EUR",
    },
  ];

  const setCurrencyState = (value) => {
    setCurrency(value);
  };

  const formatDate = (date) => {
    const formatter = new Intl.DateTimeFormat("ca-iso8601");
    const split = formatter.format(date).toString().split("/");
    return split[2] + "-" + split[1] + "-" + split[0];
  };

  const submitQuote = () => {
    if (token) {
      const formattedStartDate = formatDate(startDate);
      const formattedEndDate = formatDate(endDate);
      const ageArray = ages.split(",");
      dispatch(
        getQuote(ageArray, currency, formattedStartDate, formattedEndDate, token)
      );
    }
    clearForm();
    setShowTotal(true);
  };

  const clearForm = () => {
    setAges("");
    setCurrencyState("");
    setStartDate(new Date());
    setStartDateShow(false);
    setEndDateShow(false);
    setEndDate(new Date());
    showStartDatepicker;
    showEndDatepicker;
  };

  return (
    <SafeAreaView>
      <ScrollView>
          <Button type="logout" text="Log Out" onPress={logout} />
        <View>
          <Text style={styles.header}>Enter Details</Text>
          <View style={styles.ages}>
          <Text>Enter all ages in your party</Text>
          <FormInput placeholder="Ages" value={ages} setValue={setAges} />
          </View>
          <View style={styles.currency}>
          <Dropdown
            label="Select Currency"
            data={data}
            onChangeText={(value) => setCurrencyState(value)}
          />
          </View>
          <Button
            onPress={showStartDatepicker}
            type="DATE"
            text="Start date"
          />
          <View style={styles.date}>

          {startDateShow && (
            <DateTimePicker
              testID="dateTimePicker"
              dateFormat="longdate"
              value={startDate}
              mode={mode}
              is24Hour={true}
              display="default"
              onChange={onStartDateChange}
              
            />
          )}
          </View>
          <Button
            onPress={showEndDatepicker}
            type="DATE"
            text="End date"
            />
            <View style={styles.date}>
          {endDateShow && (
            <DateTimePicker
              testID="dateTimePicker"
              value={endDate}
              mode={mode}
              is24Hour={true}
              display="default"
              onChange={onEndDateChange}
            />
          )}
          </View>
          <Button onPress={submitQuote} text="Submit" />
        </View>

        
        { showTotal ? (

          <View >
            <Text style={styles.header}>Your Quote</Text>
            <View style={{alignItems: 'center'}} >
          <Text style={styles.quoteTotal} >
            ${quoteTotal} {quoteCurrency}
          </Text>
          <Text style={styles.quoteId}>Quote ID: {quoteId}</Text>
          </View>
        </View>
          ):null
  
          }
      </ScrollView>
    </SafeAreaView>
  );
}

const styles = StyleSheet.create({
  header: {
    fontSize: 25,
    fontWeight: "bold",
    paddingBottom: 20,
    paddingTop: 10,
    justifyContent: "center",
    alignContent: "center",
    justifyContent: "center",
    textAlign: "center",
  },
  quoteTotal: {
    alignItems: "center",
    fontWeight: 'bold',
    fontSize: 25,
    paddingBottom: 10
  },
  quoteId: {
    fontSize: 15
  },
  date: {
    marginLeft: 40,
    marginVertical: 10
  },
  dateText: {
    textAlign: "center"
  },
  ages: {
    alignItems: "center",

  },
  currency: {
    marginLeft: 40,
    padding: 25
  },
});
