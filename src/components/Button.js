import React from "react";
import { Text, StyleSheet, Pressable } from "react-native";

export default function LoginButton({ onPress, text, type = "PRIMARY" }) {
  return (
    <Pressable
      onPress={onPress}
      style={[styles.container, styles[`container_${type}`]]}
    >
      <Text style={[styles.buttonText, styles[`text_${type}`]]}>{text}</Text>
    </Pressable>
  );
}

const styles = StyleSheet.create({
  container: {
    padding: 15,
    margin: 10,
    marginVertical: 5,
    // alignItems: "center",
    borderRadius: 5,
    
  },
  container_PRIMARY: {
    backgroundColor: "#FFA500",
  },
  container_TERTIARY: {
    alignItems: "center",
    textAlign: "center",
    justifyContent: "center"
  },
  container_logout: {
    paddingTop: 5,
    paddingBottom: 0
    
  },
  text: {
    fontWeight: "bold",
    color: "white",
    fontSize: 18,
    
  },
  container_DATE: {
    backgroundColor: "white",
    margin: 40,
  },
  text_PRIMARY: {
    fontWeight: "bold",
    color: "white",
    fontSize: 18,
    textAlign: "center",

  },
  text_TERTIARY: {
    fontSize: 18,
  },
  text_DATE: {
    textAlign: "center",
  },
});
