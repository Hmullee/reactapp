import React from 'react'
import { View, TextInput, StyleSheet } from 'react-native'

export default function FormInput({value, setValue, placeholder, secureTextEntry}) {
    return (
        <View style={styles.container}>
            <TextInput 
            value={value}
            onChangeText={setValue}
            placeholder={placeholder} 
            style={styles.input}
            secureTextEntry={secureTextEntry}
            />
        </View>
    )
}

const styles = StyleSheet.create({
    container: {
        backgroundColor: '#f9fbfc',
        borderRadius: 5,
        borderWidth: 1,
        width: '75%',
        borderColor: '#e8e8e8',
        marginVertical: 10,
        alignItems: 'center'
    },
    input: {
        padding: 10,
    }
})