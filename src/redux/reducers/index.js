import React from "react";
import authReducer from "./authReducer";
import quoteReducer from "./quoteReducer";
import { combineReducers } from "redux";

// let reducers = combineReducers({
//     auth: authReducer
// })
// // const persistConfig = {
// //     key: "root",
// //     storage,
// //     whitelist: ["auth"]
// // }

// const rootReducer = (state, action) => {
//     return reducers(state, action);
// }

// export default rootReducer;

// // export default persistReducer(persistConfig, rootReducer);

const reducers = combineReducers({
  auth: authReducer,
  quote: quoteReducer,
})

export default reducers;