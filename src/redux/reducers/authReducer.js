import * as types from "../actions/types";

const initialState = {
    token: "",
    message: null,
    loading: false,
    code: null,
  };

  const authReducer = (state = initialState, action) => {
    switch (action.type) {
      case types.LOGIN_START:
      case types.REGISTER_START:
        return {
          ...state,
          loading: true,
        };
      case types.LOGIN_SUCCESS:
      case types.REGISTER_SUCCESS:
        return {
          ...state,
          loading: false,
          token: action.payload,
          isLoggedIn: true
        };
      case types.LOGIN_FAIL:
      case types.REGISTER_FAIL:
        return {
          ...state,
          loading: false,
          token: action.payload,
          isLoggedIn: false
        };
      case types.LOGOUT_USER:
        return {
          ...state,
          token: null,
          isLoggedIn: false
        };
        case types.SET_LOGIN_MESSAGE:
        return {
          ...state,
          message: action.payload,
        };
        case types.SET_LOGIN_CODE:
        return {
          ...state,
          code: action.payload,
        };
      default:
        return state;
    }
  };

  export default authReducer;