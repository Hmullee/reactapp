import { QUOTE_LOADING, UPDATE_QUOTE_ID,
  UPDATE_QUOTE_CURRENCY,
  UPDATE_QUOTE_TOTAL, } from "../actions/types";

const initialState = {
  quoteId: "",
  quoteTotal: 0,
  quoteCurrency: "",
  loading: false,
};

const quoteReducer = (state = initialState, action) => {
  switch (action.type) {
    case QUOTE_LOADING:
      return {
        ...state,
        loading: true,
      };
      case UPDATE_QUOTE_ID:
        return {
          ...state,
          quoteId: action.payload
        }
        case UPDATE_QUOTE_CURRENCY:
        return {
          ...state,
          quoteCurrency: action.payload
        }
      case UPDATE_QUOTE_TOTAL:
      return {
        ...state,
        quoteTotal: action.payload
      };
      default:
          return state;
  }
}

export default quoteReducer;