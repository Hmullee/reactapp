import axios from "axios";
import {
  QUOTE_LOADING,
  UPDATE_QUOTE_ID,
  UPDATE_QUOTE_CURRENCY,
  UPDATE_QUOTE_TOTAL,
} from "../actions/types";
import { authHeaderActions } from "./authHeaderActions";

export const getQuote = (ages, currency, startDate, endDate, token) => {
  return function (dispatch) {
    const requestOptions = {
      headers: authHeaderActions(token),
    };

    dispatch(setQuoteLoading());
    axios
      .post(
        "http://192.168.0.149/api/quotation",
        {
          ages,
          currency,
          startDate,
          endDate,
        },
        requestOptions
      )
      .then((response) => {
        dispatch({type: UPDATE_QUOTE_CURRENCY, payload: response.data.data.currency })
        dispatch({type: UPDATE_QUOTE_ID, payload: response.data.data.quote_id})
        dispatch({type: UPDATE_QUOTE_TOTAL, payload: response.data.data.quote_total})
      })
      .catch((error) => {
        console.log(error.response);
      });
  };
};

export const setQuoteLoading = () => {
  return {
    type: QUOTE_LOADING,
  };
};
