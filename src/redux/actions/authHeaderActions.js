import React from 'react'


export function authHeaderActions(user) {
    if (user) {
        return {
            Authorization: `Bearer ${user}`
        }
    } else {
        return {}
    }
}
