import axios from "axios";
import * as types from "./types";

const loginStart = () => ({
  type: types.LOGIN_START,
});

const loginSuccess = (token) => ({
  type: types.LOGIN_SUCCESS,
  payload: token,
});

const loginFail = (error) => ({
  type: types.LOGIN_FAIL,
  payload: error,
});

export const logoutUser = () => ({
  type: types.LOGOUT_USER,
});

const registerStart = () => ({
  type: types.REGISTER_START,
});

const registerSuccess = (token) => ({
  type: types.REGISTER_SUCCESS,
  payload: credentials,
});

const registerFail = (error) => ({
  type: types.REGISTER_FAIL,
  payload: error,
});

export const loginInitiate = (email, password) => {
  return function (dispatch) {
    dispatch(loginStart());
    axios
      .post("http://192.168.0.149/api/login", {
        email,
        password,
      })
      .then((response) => {
        if (response.data.code === 200) {
          dispatch(loginSuccess(response.data.data.token));
          dispatch({ type: types.SET_LOGIN_CODE, payload: response.data.code });
          dispatch({
            type: types.SET_LOGIN_MESSAGE,
            payload: response.data.message,
          });
        } else {
          console.log("error in else" + response.data.code);
          dispatch(loginFail(response.data.data));
          dispatch({ type: types.SET_LOGIN_CODE, payload: response.data.code });
          dispatch({
            type: types.SET_LOGIN_MESSAGE,
            payload: response.data.message,
          });
        }
      })
      .catch((error) => {
        console.log("error", error);
        dispatch(loginFail(error.response));
      });
  };
};

export const registerInitiate = (name, email, password) => {
  return function (dispatch) {
    dispatch(registerStart());
    axios
      .post("http://192.168.0.149/api/register", {
        name,
        email,
        password,
      })
      .then((response) => {
        if (response.data.code === 200) {
          dispatch(registerSuccess(response.data.data.token));

        } else {
          dispatch(registerFail(response.data))
        }
        console.log("response", response);
      })
      .catch((error) => {
        console.log("error", error.response);
        dispatch(registerFail(error.response));
      });
  };
};
