
import React from 'react';
import {store, persistor} from "./src/redux/store";
import { Provider } from "react-redux";
import Navigation from "./navigation";
import { PersistGate } from "redux-persist/integration/react";

export default function App() {
  return (
        <Navigation />
  )
}
